﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
	private int hp;
	private float moveSpeed;

	private Player player;

	void Start () 
	{
		hp = 3;
		moveSpeed = 0.5f;

		player = GameObject.FindWithTag ("Player").GetComponent<Player>();
	}

	void Update () 
	{
		Vector3 toTarget = player.transform.position - transform.position;
		transform.Translate (toTarget * moveSpeed * Time.deltaTime);
	}

	public void Hit(int value)
	{
		hp -= value;

		if (hp <= 0) Die ();
	}

	void Die()
	{
		gameObject.SetActive (false);
	}
}
