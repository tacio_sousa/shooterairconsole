﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;

public class Player : MonoBehaviour 
{
	public GameObject shot;

	private float moveSpeed;
	private int tickShot, totalTickShot;
	private string messageState;
	private List<GameObject> shots = new List<GameObject>();

	void Start () 
	{
		moveSpeed = 0.1f;
		totalTickShot = 10;
		tickShot = totalTickShot;
		messageState = "stop";

		AirConsole.instance.onMessage += OnMessage;

		ShotsObjectPool ();
	}

	void ShotsObjectPool()
	{
		for (int i = 0; i < 30; i++)
		{
			GameObject newShot = Instantiate (shot, new Vector2(-15, 0), transform.rotation);
			shots.Add (newShot);
			shots [i].SetActive (false);
		}
	}

	void Update () 
	{
		//move
		if (messageState == "up" || Input.GetKey (KeyCode.W)) MoveUp ();
		if (messageState == "down" || Input.GetKey (KeyCode.S)) MoveDown ();
		if (messageState == "left" || Input.GetKey (KeyCode.A)) MoveLeft ();
		if (messageState == "right" || Input.GetKey (KeyCode.D))  MoveRight ();

		//shot
		if (messageState == "fireUp" || messageState == "fireDown" || messageState == "fireLeft" || messageState == "fireRight" || 
			Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) ||
			Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) )
		{
			tickShot++;

			if (tickShot >= totalTickShot) 
			{
				tickShot = 0;

				if (messageState == "fireUp" || Input.GetKey (KeyCode.UpArrow)) FireUp ();
				else if (messageState == "fireDown" || Input.GetKey (KeyCode.DownArrow)) FireDown ();
				else if (messageState == "fireLeft" || Input.GetKey (KeyCode.LeftArrow)) FireLeft ();
				else if (messageState == "fireRight" || Input.GetKey (KeyCode.RightArrow)) FireRight ();
			}
		}

		if (messageState == "fireStop"||  Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) ||
			Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) )
		{
			tickShot = totalTickShot;
		}

		/*if (Input.GetMouseButton(0)) 
		{
			tickShot++;

			if (tickShot >= totalTickShot) 
			{
				tickShot = 0;
				FireUp ();
				//FireMouse ();
			}
		}
		if (Input.GetMouseButtonUp(0)) 
		{
			tickShot = totalTickShot;
		}*/
	}

	void OnMessage (int device_id, JToken data)
	{
		messageState = (string)data;
	}

	void MoveUp()
	{
		Move (0, moveSpeed);
	}
	void MoveDown()
	{
		Move (0, -moveSpeed);
	}
	void MoveLeft()
	{
		Move (-moveSpeed, 0);
	}
	void MoveRight()
	{
		Move (moveSpeed, 0);
	}

	void Move(float x, float y)
	{
		transform.Translate (new Vector3(x, y));
	}

	void FireUp()
	{
		Fire (0, 1);
	}
	void FireDown()
	{
		Fire (0, -1);
	}
	void FireLeft()
	{
		Fire (-1, 0);
	}
	void FireRight()
	{
		Fire (1, 0);
	}

	GameObject GetShotInPool()
	{
		for (int i = 0; i < shots.Count; i++)
		{
			if (shots [i].activeSelf == false) 
			{
				return shots [i];
				break;
			}
		}

		return null;
	}

	void Fire (float x, float y)
	{
		GameObject newShot = GetShotInPool();
		newShot.transform.position = transform.position;
		GetShotInPool().GetComponent<Shot> ().Move (x , y);
	}

	/*void FireMouse()
	{
		Vector3 pos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10);
		pos = Camera.main.ScreenToWorldPoint (pos);

		GameObject newShot = Instantiate (shot, transform.position, transform.rotation);
		newShot.transform.LookAt (pos);
	}*/
}
