﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour 
{
	public Rigidbody2D rb;

	private int damage;
	private float moveSpeed = 1000;

	void Start () 
	{
		damage = 1;
		//rb.AddForce (rb.transform.forward * 1000);
	}

	void Update () 
	{
		
	}

	public void Move(float xForce, float yForce)
	{
		gameObject.SetActive (true);
		rb.AddForce (new Vector2(xForce * moveSpeed, yForce * moveSpeed));
		Invoke ("Disable", 2f);
	}

	void Disable()
	{
		gameObject.SetActive (false);
	}

	void OnTriggerEnter2D(Collider2D obj)
	{
		if (obj.gameObject.tag == "Enemy") 
		{
			obj.GetComponent<Enemy> ().Hit (damage);
			gameObject.SetActive (false);
		}
	}
}
